﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Authentication;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.WebApi.Authentication;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.WebApi.Error;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper
{
    class Program
    {
        static void Main(string[] args)
        {
            WebHost.CreateDefaultBuilder()
               .ConfigureLogging(ConfigureLogging)
               .ConfigureServices(services =>
               {
                   services.AddMvc();
                   services.AddAuthentication("BasicAuthentication")
                   .AddScheme<AuthenticationSchemeOptions, BasicAuthenticationHandler>("BasicAuthentication", null);

                   services.AddScoped<IUserService, UserService>();
               })
               .Configure(app => {
                   app.UseCors(x => x
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());

                   app.UseMiddleware(typeof(ErrorHandling));
                   app.UseAuthentication();

                   app.UseMvc();
               })
               ///<summary>
               ///  Web API port number : 10500
               ///</summary>
               .UseUrls("http://*:10500")
               .Build()
               .Run();
        }

        private static void ConfigureLogging(WebHostBuilderContext hostingContext, ILoggingBuilder logging)
        {
            logging.ClearProviders();
        }

    }
}
