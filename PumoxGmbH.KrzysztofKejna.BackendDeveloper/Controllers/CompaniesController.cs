﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Ninject;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.WebApi.Bootstrapp;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.WebApi.Controllers
{
    [Authorize]
    [Route("company")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly ICompanyService _companyService;

        public CompaniesController()
        {
            var kernel = DependencyResolver.GetKernel();

            _companyService = kernel.Get<ICompanyService>();
        }

        [HttpPost("create")]
        public long PostCompany([FromBody]Company company)
        {
            _companyService.CompanyValidator(company);
            var result = _companyService.AddCompany(company);
            return result;
        }

        [AllowAnonymous]
        [HttpPost("search")]
        public List<SearchResult> GetCompanies([FromBody]SearchItem search)
        {
            var result = _companyService.GetCompany(search);
            return result;
        }

        [HttpPut("update/{id}")]
        public void UpdateCompany(int id, [FromBody]Company company)
        {
            _companyService.CompanyValidator(company);
            _companyService.UpdateCompany(id, company);
        }

        [HttpDelete("delete/{id}")]
        public void DeleteCompany(int id)
        {
            _companyService.DeleteCompany(id);
        }
    }
}
