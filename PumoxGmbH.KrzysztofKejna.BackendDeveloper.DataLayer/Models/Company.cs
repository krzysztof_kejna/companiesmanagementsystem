﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models
{
    public class Company
    {
        [Key]
        public long Id { get; set; }
        public string Name { get; set; }
        public int? EstablishmentYear { get; set; }
        public List<Employee> Employees { get; set; }
    }
}
