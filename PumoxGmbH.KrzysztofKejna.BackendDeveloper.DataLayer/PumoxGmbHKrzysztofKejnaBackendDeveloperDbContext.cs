﻿using Microsoft.EntityFrameworkCore;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer
{
    public interface IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext : IDisposable
    {
        DbSet<Company> Companies { get; set; }
        DbSet<Employee> Employees { get; set; }
        int SaveChanges();
    }

    public class PumoxGmbHKrzysztofKejnaBackendDeveloperDbContext : DbContext, IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext
    {
        /// <summary>
        /// Database name: PumoxGmbHKrzysztofKejnaBackendDeveloperDb
        /// </summary>
        private const string _connectionString = "Data Source=.;Initial Catalog=PumoxGmbHKrzysztofKejnaBackendDeveloperDb;Integrated Security=True;";
        public DbSet<Company> Companies { get; set; }
        public DbSet<Employee> Employees { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
