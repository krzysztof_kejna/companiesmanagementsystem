﻿using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Bootstrapp
{
    public class DataLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext>().ToMethod(x => new PumoxGmbHKrzysztofKejnaBackendDeveloperDbContext());
        }
    }
}
