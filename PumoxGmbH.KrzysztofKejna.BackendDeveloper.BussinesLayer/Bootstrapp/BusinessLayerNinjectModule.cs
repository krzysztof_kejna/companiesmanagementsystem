﻿using Ninject.Modules;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Bootstrapp
{
    public class BusinessLayerNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICompanyService>().To<CompanyService>();
            Kernel.Bind<IEmployeeService>().To<EmployeeService>();
            Kernel.Bind<IUserService>().To<UserService>();
        }
    }
}
