﻿using Microsoft.EntityFrameworkCore;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services
{
    public interface ICompanyService
    {
        long AddCompany(Company company);
        void DeleteCompany(int id);
        List<SearchResult> GetCompany(SearchItem search);
        void UpdateCompany(int id, Company company);
        void CompanyValidator(Company company);
    }

    public class CompanyService : ICompanyService
    {
        private readonly Func<IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext> _dbContextFactory;
        private readonly IEmployeeService _employeeService;

        public CompanyService(
            Func<IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext> dbContextFactory,
            IEmployeeService employeeService)
        {
            _dbContextFactory = dbContextFactory;
            _employeeService = employeeService;
        }

        public long AddCompany(Company company)
        {
            using (var context = _dbContextFactory())
            {
                var companyEntity = context.Companies.Add(company);
                context.SaveChanges();
                return companyEntity.Entity.Id;
            }
        }

        public List<SearchResult> GetCompany(SearchItem search)
        {
            var results = new List<SearchResult>();
            using (var context = _dbContextFactory())
            {
                var companies = context.Companies
                    .Where(c => c.Name.Contains(search.Keyword) || c.Employees.Any(e => e.FirstName.Contains(search.Keyword) || e.LastName.Contains(search.Keyword)));
              
                foreach(var company in companies)
                {
                    var employeedList = _employeeService.GetEmployeesByCompanyId(company.Id, search);
                    var result = new SearchResult
                    {
                        Name = company.Name,
                        EstablishmentYear = company.EstablishmentYear.Value,
                        Employees = employeedList
                    };
                    results.Add(result);
                }

                return results;
            }
        }

        public void UpdateCompany(int id, Company company)
        {
            company.Id = id;

            using (var context = _dbContextFactory())
            {
                context.Companies.Update(company);
                context.SaveChanges();
            }
        }

        public void DeleteCompany(int id)
        {
            using (var context = _dbContextFactory())
            {
                var company = context.Companies.FirstOrDefault(c => c.Id == id);
                context.Companies.Remove(company);
                context.SaveChanges();
            }
        }

        public void CompanyValidator(Company company)
        {
            Validator(company.Name);
            Validator(company.EstablishmentYear);
            Validator(company.Employees);
            foreach (var employee in company.Employees)
            {
                Validator(employee.FirstName);
                Validator(employee.LastName);
                Validator(employee.DateOfBirth);
                Validator(employee.JobTitle);
            }
        }

        private void Validator(object item)
        {
            if (item == null)
            {
                throw new ArgumentNullException($"One of required value for this request is missing");
            }
        }
    }
}
