﻿using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
    }

    public class UserService : IUserService
    {
        /// <summary>
        /// List of authorized user for basic authentication
        /// </summary>
        /// <returns>
        /// user name: user, password: password
        /// </returns> 

        private List<User> _users = new List<User>
        {
            new User { Id = 1, Username = "user", Password = "password" }
        };

        public User Authenticate(string username, string password)
        {
            return _users.SingleOrDefault(x => x.Username == username && x.Password == password);
        }
    }
}
