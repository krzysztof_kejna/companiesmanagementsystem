﻿using PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer;
using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Services
{
    public interface IEmployeeService
    {
        List<EmployeeItem> GetEmployeesByCompanyId(long id, SearchItem search);
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly Func<IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext> _dbContextFactory;

        public EmployeeService(
            Func<IPumoxGmbHKrzysztofKejnaBackendDeveloperDbContext> dbContextFactory)
        {
            _dbContextFactory = dbContextFactory;
        }

        public List<EmployeeItem> GetEmployeesByCompanyId(long id, SearchItem search)
        {
            using(var context = _dbContextFactory())
            {
                var employeeList = context.Employees
                    .Where(e => e.CompanyId == id);

                if (search.EmployeeDateOfBirthFrom != null)
                {
                    employeeList = employeeList.Where(e => e.DateOfBirth >= search.EmployeeDateOfBirthFrom);

                }

                if (search.EmployeeDateOfBitrhTo != null)
                {
                    employeeList = employeeList.Where(e => e.DateOfBirth <= search.EmployeeDateOfBirthFrom);
                }

                if (search.EmployeeJobTitles != null)
                {
                    foreach (var EmployeeJobTitle in search.EmployeeJobTitles)
                    {

                        employeeList = employeeList.Where(e => e.JobTitle == EmployeeJobTitle);

                    }
                }
                var result = GetValidEmployeeItem(employeeList);

                return result;
            }
        }

        public List<EmployeeItem> GetValidEmployeeItem(IQueryable<Employee> employeesQueryBase)
        {
            var resultItems = employeesQueryBase
                .Select(employeeItem => new EmployeeItem
                {
                    FirstName = employeeItem.FirstName,
                    LastName = employeeItem.LastName,
                    DateOfBirth = employeeItem.DateOfBirth.Value,
                    JobTitle = employeeItem.JobTitle.ToString()
                })
                .ToList();

            return resultItems;
        }
    }
}
