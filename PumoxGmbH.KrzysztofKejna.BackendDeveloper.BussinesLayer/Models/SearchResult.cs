﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models
{
    public class SearchResult
    {
        public string Name;
        public int EstablishmentYear;
        public List<EmployeeItem> Employees;
    }
}
