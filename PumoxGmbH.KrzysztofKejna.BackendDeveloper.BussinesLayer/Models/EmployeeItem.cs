﻿using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models
{
    public class EmployeeItem
    {
        public string FirstName;
        public string LastName;
        public DateTime DateOfBirth;
        public string JobTitle;
    }
}
