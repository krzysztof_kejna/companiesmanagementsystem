﻿using PumoxGmbH.KrzysztofKejna.BackendDeveloper.DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PumoxGmbH.KrzysztofKejna.BackendDeveloper.BusinessLayer.Models
{
    public class SearchItem
    {
        public string Keyword;
        public DateTime? EmployeeDateOfBirthFrom;
        public DateTime? EmployeeDateOfBitrhTo;
        public List<JobTitle?> EmployeeJobTitles;
    }
}
