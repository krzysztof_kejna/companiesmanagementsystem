CREATE DATABASE PumoxGmbHKrzysztofKejnaBackendDeveloperDb
GO

USE PumoxGmbHKrzysztofKejnaBackendDeveloperDb;

CREATE TABLE Companies(
   [Id] bigint IDENTITY(1,1) PRIMARY KEY ,
   [Name] varchar(255),
   [EstablishmentYear] int,
);

CREATE TABLE Employees(
   [Id] bigint IDENTITY(1,1) PRIMARY KEY,
   [FirstName] varchar(255),
   [LastName] varchar(255),
   [DateOfBirth] Date,
   [CompanyId] bigint,
   [JobTitle] int,
   FOREIGN KEY (CompanyId) REFERENCES Companies(Id)
   ON DELETE CASCADE
)